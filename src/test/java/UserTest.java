import org.junit.jupiter.api.Test;
import user.registration.User;
import static org.junit.jupiter.api.Assertions.*;

public class UserTest {
    @Test
    void testSameUser() {
        User user = new User("alex@gmail.com",8);
        assertEquals("alex@gmail.com",user.getEmail()); // Test nul, mais ça marche...
    }
}

// Test avec mockito : mockito.mock, mockito.when, mockito.verify