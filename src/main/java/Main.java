import user.registration.RegisterService;
import user.registration.User;
import user.registration.UserRepository;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Veuillez entrer votre adresse email");
        Scanner sc = new Scanner(System.in); // On crée la saisie utilisateur
        RegisterService registerService = new RegisterService(); // On instancie la méthode pour enregistrer un utilisateur
        int register1=registerService.register(sc.next()); // On enregistre dans une variable le résultat de l'entrée utilisateur
        System.out.println(register1); // On affiche la clef générée
    }
}

// Problème perso : oublie de créer des instances d'objets + oublie d'enregistrer les résultats dans des variables.
