package user.registration;

public class User {
    private String email = "";
    private int secretKey = 0;

    public User(String email, int secretKey) {
        this.email = email;
        this.secretKey = secretKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(int secretKey) {
        this.secretKey = secretKey;
    }
}
