package user.registration;

import java.util.Random;

public class RandomKeyGenerator {

    Random nbRandom = new Random();

    public RandomKeyGenerator() {

    }

    public int generateKey() {
        int key;
        key = nbRandom.nextInt(0, 10);
        return key;
    }
}
