package user.registration;

public class RegisterService {

    UserRepository userRepository = new UserRepository();

    public RegisterService() {

    }

    public int register(String email) {
        /* Cette méthode ne sera pas retenue définitivement dans l'application,
        contrairement à UserRepository qui sera conservé tout du long de la vie de l'application.
        */
        // On veut récupérer la clef générée
        RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(); // On instancie un objet;
        int key = randomKeyGenerator.generateKey(); // On assigne à une variable, la valeur de l'appel de la méthode

        // On veut enregistrer l'utilisateur dans le repertoire
        User user = new User(email, key);
        this.userRepository.save(user);

        // On veut retourner la clef secrete
        return key;
    }
}
